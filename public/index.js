var cartaJogador;
var cartaMaquina;
var pilhaMaquina;
var pilhaJogador;
let contadorJogador = 0;
let contadorMaquina = 0;

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

async function buscaHeroAPI() {
  try {
    let id = getRandomInt(731);
    const url = `https://akabab.github.io/superhero-api/api/id/${id}.json`;

    var data = await fetch(url)
    const result = await data.json();
    return result;
  } catch (error) { 
    return await buscaHeroAPI()
  }
}

async function sortearCarta() {
  document.getElementById("btnSortear").disabled = true;
  pilhaMaquina = new stack();
  pilhaJogador = new stack();

  for (let i = 0; i < 10; i++) {
    const hero = await this.buscaHeroAPI();
    const carta = {
      nome: hero.name,
      imagem: hero.images.lg,
      atributos: {
        intelligence: hero.powerstats.intelligence,
        strength: hero.powerstats.strength,
        speed: hero.powerstats.speed,
        durability: hero.powerstats.durability,
        power: hero.powerstats.power,
        combat: hero.powerstats.combat
      }
    }
    if (i < 5) {
      pilhaMaquina.push(carta);
    } else {
      pilhaJogador.push(carta);
    }
  }
  cartaJogador =  pilhaJogador.pop();
  exibirCartaJogador(cartaJogador);
  
  cartaMaquina =  pilhaMaquina.pop();
  document.getElementById("btnJogar").disabled = false;
}


function obtemAtributoSelecionado() {
  var radioAtributo = document.getElementsByName("atributo");
  for (var i = 0; i < radioAtributo.length; i++) {
    if (radioAtributo[i].checked) {
      return radioAtributo[i].value;
    }
  }
}


function jogar() {
  exibirCartaMaquina(cartaMaquina);

  var atributoSelecionado = obtemAtributoSelecionado();
  var divPontosJogador = document.getElementById("pontosJogador");
  var divResultado = document.getElementById("resultado");
  var divPontosMaquina = document.getElementById("pontosMaquina");
  
  if (
    cartaJogador.atributos[atributoSelecionado] >
    cartaMaquina.atributos[atributoSelecionado]
  ) {
    contadorJogador += 1;
    htmlResultado = "<p class='resultado-final'>Venceu</p>";
  } else if (
    cartaJogador.atributos[atributoSelecionado] <
    cartaMaquina.atributos[atributoSelecionado]
  ) {
    contadorMaquina += 1;
    htmlResultado = "<p class='resultado-final'>Perdeu</p>";
  } else {
    htmlResultado = "<p class='resultado-final'>Empatou, na dúvida a maquina supera o Homem. Ponto para a maquina</p>";
    contadorMaquina += 1;
  }
  divPontosJogador.innerHTML = contadorJogador.toString();
  divResultado.innerHTML= htmlResultado;
   divPontosMaquina.innerHTML =  contadorMaquina.toString();

  document.getElementById("btnJogar").disabled = true;
  document.getElementById("btnAvancar").disabled = false;
  let somaPontos = contadorJogador + contadorMaquina;

  if(somaPontos === 5 && contadorJogador > contadorMaquina){
    htmlResultado = "<p class='resultado-final'>OS HOMENS SUPERAM AS MÁQUINAS</p>";
    divResultado.innerHTML= htmlResultado;
  }
   if(somaPontos === 5 && contadorMaquina > contadorJogador) {
    htmlResultado = "<p class='resultado-final'>AS MAQUINAS SUPERAR OS HOMENS</p>";
    divResultado.innerHTML= htmlResultado;
  }
  return;
}

function avancar() {
  document.getElementById("btnJogar").disabled = false;
  document.getElementById("btnAvancar").disabled = true;
  cartaJogador =  pilhaJogador.pop();
  cartaMaquina =  pilhaMaquina.pop();
  exibirCartaJogador(cartaJogador);
  limpaCartaMaquina();
  return;
}

function exibirCartaJogador(cartaJogador) {
  var divCartaJogador = document.getElementById("carta-jogador");

  divCartaJogador.style.backgroundImage = "url(" + cartaJogador.imagem + ")"

  var nome = `<p class="carta-subtitle">${cartaJogador.nome}</p>`;
  var tagHTML = "<div id='opcoes' class='carta-status'>";
  var opcoesTexto = "";

  for (var atributo in cartaJogador.atributos) {
    opcoesTexto +=
      "<input type='radio' name='atributo' value='" + atributo + "' name='opcoes'>" + 
      atributo + " " + cartaJogador.atributos[atributo] + "<br>";
  }
  divCartaJogador.innerHTML =  nome + tagHTML + opcoesTexto + "</div>";
}



function exibirCartaMaquina(cartaMaquina) {
  var divCartaMaquina = document.getElementById("carta-maquina");
  var nome = `<p class="carta-subtitle">${cartaMaquina.nome}</p>`;
  divCartaMaquina.style.backgroundImage = "url(" + cartaMaquina.imagem + ")"
  var tagHTML = "<div id='opcoes' class='carta-status'>";

  var opcoesTexto = "";
  for (var atributo in cartaMaquina.atributos) {
    opcoesTexto +=
      "<p type='text' name='atributo' value='" + atributo + "'>" + atributo + " " +
      cartaMaquina.atributos[atributo] +"</p>";
  }
  divCartaMaquina.innerHTML =  nome + tagHTML + opcoesTexto + "</div>";
}

function limpaCartaMaquina() {
  var divCartaMaquina = document.getElementById("carta-maquina");
  var nome = "";
  divCartaMaquina.style.backgroundImage = "";
  var tagHTML = "";
  var opcoesTexto = "";
  divCartaMaquina.innerHTML =  nome + tagHTML + opcoesTexto + "</div>";
}

addEventListener(jogar(), (e)=>{
  if(!document.querySelector("input[name='opcoes']").checked){
     alert("Marque o radio button");
     e.preventDefault();
  }
});

const element = document.getElementById("btnJogar");
element.addEventListener("click", semAtributo);

function semAtributo() {
  if (querySelector(!document.getElementById("input[name='opcoes']"))){
    alert("Marque o radio button");
  }
}